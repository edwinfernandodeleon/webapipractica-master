var database = require('../config/database.config');
var Usuario = {};

Usuario.selectAll = function(callback) {
  if(database) {
    database.query("SELECT * FROM Usuario ORDER BY cambios_contrasena DESC",
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(resultados);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

Usuario.autenticar = function(data, callback) {
  if(database) {
    var sql = "CALL sp_autenticarUsuario(?,?)";
    database.query(sql, [data.nick, data.contrasena],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, resultado);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll


Usuario.insert = function(data, callback) {
  if(database) {
    database.query("CALL sp_insertUsuario(?,?)", [data.nick, data.contrasena],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        var respuesta = {
          insertId: resultado.insertId,
          nick: data.nick,
          idUsuario: resultado.insertId
        };
        callback(null, respuesta);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

Usuario.update = function(data, callback) {
  if(database) {
    var sql = "CALL sp_updateUsuario(?,?,?,?)";
    database.query(sql,
    [data.nick, data.contrasena, data.picture, data.idUsuario],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

Usuario.delete = function(idUsuario, callback) {
  if(database) {
    var sql = "CALL sp_deleteUsuarioTarea(?)";
    database.query(sql, idUsuario,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"Mensaje": "Eliminado"});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll


module.exports = Usuario;
