var database = require('../config/database.config');
var Tarea = {}

Tarea.selectAll = function(idUsuario, callback) {
  if(database) {
    var sql = "CALL sp_verTareaPorId(?)";
    database.query(sql, idUsuario,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, resultado);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

Tarea.insert = function(data, callback) {
  if(database) {
    database.query("sp_createTarea(?,?,?,?)", [data.titulo, data.descripcion, data.fecha_final, data.idUsuario],
    function(error, resultado) {
      if(error) throw error;
      callback({"insertId": resultado.insertId});
    });
  }
}

module.exports = Tarea;
