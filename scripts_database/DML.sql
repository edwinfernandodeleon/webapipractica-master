/*
SELECT DATE_FORMAT(fecha_registro, '%d-%c-%y') AS 'Fecha',
  DATE-FORMAT(fecha_registro,'%h:%i:%s') AS 'Hora'
FROM Tarea;
*/

USE Practica2017;

CALL sp_insertUsuario ('a' , 'a');
CALL sp_insertUsuario ('b' , 'a');
CALL sp_insertUsuario ('c' , 'a');
CALL sp_insertUsuario ('d' , 'a');
CALL sp_insertUsuario ('e' , 'a');

CALL sp_createTarea ('una' , 'a', '2017-9-1', 1);
CALL sp_createTarea ('dos' , 'a', '2017-9-1', 1);
CALL sp_createTarea ('tres' , 'a', '2017-9-1', 2);
CALL sp_createTarea ('cuatro' , 'a', '2017-9-1', 3);
CALL sp_createTarea ('cinco' , 'a', '2017-9-1', 1);
